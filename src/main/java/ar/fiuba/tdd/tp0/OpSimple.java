package ar.fiuba.tdd.tp0;

import java.util.LinkedList;
import java.util.Queue;


public class OpSimple implements IResolveOperation{
	
	Queue<Float> queue;
	IOperation operation;
	float firstNum;
	float otherNum;
	float result;
	
	public OpSimple(Queue<Float> queue, IOperation operation) {
		this.operation = operation;
		this.queue = queue;
	}

	public void operate() {
		
		java.util.Iterator<Float> iter = queue.iterator();
		int posicion = 0;
		otherNum = iter.next();
		while (iter.hasNext()) {
			firstNum = otherNum;
			otherNum = iter.next();
			posicion += 1;
		}
		
		//Si solo hay 1 solo numero en la cola, no se puede hacer ninguna operacion
		while(posicion<1){
			throw new IllegalArgumentException();
		}
		
		result = operation.doOperation(firstNum,otherNum);
		
		Queue<Float> queueAux = new LinkedList<Float>();
		for (int i=0; i< (posicion - 1); i++){
			queueAux.add(queue.remove());
		}
		queue.remove();
		queue.remove();
		
		java.util.Iterator<Float> iter2 = queueAux.iterator();
		while (iter2.hasNext()) {
			queue.add(iter2.next());
		}
		queue.add(result);
	}
}
