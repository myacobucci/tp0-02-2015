package ar.fiuba.tdd.tp0;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

public class RPNCalculator {
	
	Queue<Float> queue = new LinkedList<Float>();
	HashMap<String,IResolveOperation>  map = new HashMap<String,IResolveOperation>();

    public float eval(String expression) {

    	while (expression == null){
    		throw new IllegalArgumentException();
    	}   	
    	while (expression.length() == 0){
    		throw new IllegalArgumentException();
    	}
    	
    	String operands = "++--**//MOD";
    	String[] parts = expression.split(" ");
    	
    	//Check if first argument is an operand
		while(operands.indexOf(parts[0]) != -1){
			throw new IllegalArgumentException();
		}
		
		initializeMap();
		
    	for (int i = 0; i< parts.length; i++){
    		while(!map.containsKey(parts[i])){
    			throw new IllegalArgumentException();
    		}
    		map.get(parts[i]).operate();
    	}
    	
    	return queue.remove();
    }

	private void initializeMap() {
		for (int i=-100; i<100; i++){
			final int num = i;
			map.put(String.valueOf(i), ()->queue.add((float) num));
		}
		IOperation sum = (float firstNumber, float otherNumber) -> firstNumber + otherNumber;
		IOperation sub = (float firstNumber, float otherNumber) -> firstNumber - otherNumber;
		IOperation mult = (float firstNumber, float otherNumber) -> firstNumber * otherNumber;
		IOperation div = (float firstNumber, float otherNumber) -> firstNumber / otherNumber;
		IOperation mod = (float firstNumber, float otherNumber) -> firstNumber % otherNumber;
		map.put("+", new OpSimple(queue, sum));
		map.put("-", new OpSimple(queue, sub));
		map.put("*", new OpSimple(queue, mult));
		map.put("/", new OpSimple(queue, div));
		map.put("MOD", new OpSimple(queue, mod));
		map.put("++", new OpMultiple(queue, sum));
		map.put("--", new OpMultiple(queue, sub));
		map.put("**", new OpMultiple(queue, mult));
		map.put("//", new OpMultiple(queue, div));
	}
}
