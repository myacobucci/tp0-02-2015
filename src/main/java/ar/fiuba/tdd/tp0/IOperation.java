package ar.fiuba.tdd.tp0;

public interface IOperation{
	public float doOperation(float firstNum, float otherNum);
}