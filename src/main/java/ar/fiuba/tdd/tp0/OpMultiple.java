package ar.fiuba.tdd.tp0;

import java.util.Queue;

public class OpMultiple implements IResolveOperation{
	
	float otherNumber;
	Queue<Float> queue;
	IOperation operation;
	float result;
	
	public OpMultiple(Queue<Float> queue, IOperation operation) {
		this.queue = queue;
		this.operation = operation;
	}

	public void operate() {
		result = queue.remove();
		while (!(queue.isEmpty())){
			otherNumber = queue.remove();
			result = operation.doOperation(result,otherNumber);	
		}
		queue.add(result);
	}
}
